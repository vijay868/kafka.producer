﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace kafka.providers.IocConfig
{
    public static class ServiceExtensions
    {
        public static void RegisterProviders(this IServiceCollection services)
        {
            services.AddSingleton<KafkaProducer>();
            services.AddHostedService<KafkaProducerHostedService>();
        }
    }
}
