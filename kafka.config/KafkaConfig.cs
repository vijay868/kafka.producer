﻿using System;

namespace kafka.config
{
    public class KafkaConfig
    {
        public string BoostrapServers { get; set; }
    }
}
