﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace kafka.providers
{
    public class KafkaProducerHostedService : IHostedService
    {
        private KafkaProducer _kafkaProducer;
        private Timer _timer;
        public KafkaProducerHostedService(KafkaProducer kafkaProducer)
        {
            this._kafkaProducer = kafkaProducer;            
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(Process, null, TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(15));
            return Task.CompletedTask;
        }

        private void Process(object state)
        {
            Task.Run(async () =>
            {
                await _kafkaProducer.SendMessage($"MESSAGE AT {DateTime.Now}");
            });            
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
    }
}
