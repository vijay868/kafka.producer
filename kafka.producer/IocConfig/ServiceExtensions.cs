﻿using kafka.config;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace kafka.producer.IocConfig
{
    public static class ServiceExtensions
    {
        public static void RegisterConfiguration(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<KafkaConfig>(configuration.GetSection("KafkaConfig"));
        }
    }
}
