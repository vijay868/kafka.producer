﻿using Confluent.Kafka;
using kafka.config;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kafka.providers
{
    public class KafkaProducer
    {
        private KafkaConfig _kafkaConfig { get; set; }
        public KafkaProducer(IOptions<KafkaConfig> kafkaConfig)
        {
            this._kafkaConfig = kafkaConfig.Value;
        }

        public async Task SendMessage(string message, Action<DeliveryResult<Null, string>> action = null)
        {
            var producerConfig = new ProducerConfig {
                BootstrapServers = _kafkaConfig.BoostrapServers                
            };
            
            using(var producer = new ProducerBuilder<Null, String>(producerConfig).Build())
            {
                var dr = await producer.ProduceAsync(
                    "timemanagement_booking", 
                    new Message<Null, string> { Value = message });

                Console.WriteLine($"Delivered '{dr.Value}' to '{dr.TopicPartitionOffset}'");

                action.Invoke(dr);
            }
        }
    }
}
